<?php

class Connection{
	function __construct($dbhost, $dbuser, $dbpass, $dbname){
		$this->dbhost = $dbhost;
		$this->dbuser = $dbuser;
		$this->dbpass = $dbpass;
		$this->dbname = $dbname;

		return mysqli_connect(
			$this->dbhost,
			$this->dbuser,
			$this->dbpass,
			$this->dbname
			) or die("Not connected");
	}
}

class DataBaseConfig {
	public $dbhost;
	public $dbuser;
	public $dbpass;
	public $dbname;
	public function __construct($dbhost, $dbuser, $dbpass, $dbname){
		$this->dbhost = $dbhost;
		$this->dbuser = $dbuser;
		$this->dbpass = $dbpass;
		$this->dbname = $dbname;
	}
}

class DataBaseObject {

	protected static $dbTable;
	private $dbConfig;
	private $connection = '';
	//public $connection = '';

	public function __construct($dbConfig){
		$this->dbConfig = $dbConfig;
	 	
	 	$this->connection = mysqli_connect($dbConfig->dbhost,
	 										$dbConfig->dbuser,
	 										$dbConfig->dbpass,
	 										$dbConfig->dbname);
	}

	public function query($fields){
		$sql = 'SELECT '.$fields.' FROM '.static::$dbTable;
		$query = mysqli_query($this->connection, $sql);
		// if($query == null){
		// 	echo "query is null in queryAll()";
		// 	return;
		// }

		while($arr = mysqli_fetch_assoc($query)){
			$result[] =  $arr;
		}

		return $result;
	}

	public function queryAllToJson(){
		return json_encode($this->query('*'));
	}
}

class Restaurant extends DataBaseObject{

	protected static $dbTable = "restaurant";
	//public $connection = '';
/*
	 function __construct($dbhost, $dbuser, $dbpass, $dbname){
	 	$this->connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	}
	*/

/*
	public function getRestaurant(){
		$sql = "SELECT logo, name, rating FROM restaurant";
		$query = mysqli_query($this->connection, $sql);
		
		while($arr = mysqli_fetch_assoc($query)){
			$result[] =  $arr;
		}

		return $result;
	}
	*/

	//public function 
}

$db = new DataBaseConfig('127.0.0.1', 'root', '', 'restaurant_db');
$rest = new Restaurant($db);
$s = $rest->queryAllToJson();
echo $s;

//comment


